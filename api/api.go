package api

import (
	go_pg "github.com/go-pg/pg"

	"github.com/muscar/muninn-go/models"
)

type FlagApi struct {
	db *go_pg.DB
}

func NewFlagApi(db *go_pg.DB) *FlagApi {
	return &FlagApi{db: db}
}

func (api *FlagApi) All() ([]models.Flag, error) {
	var flags []models.Flag
	err := api.db.Model(&flags).Select()
	return flags, err
}

func (api *FlagApi) Get(name string) (*models.Flag, error) {
	var flag models.Flag
	err := api.db.Model(&flag).Where("name = ?", name).Select()
	if err == go_pg.ErrNoRows {
		return nil, err
	}
	return &flag, err
}

func (api *FlagApi) Add(flag *models.Flag) error {
	return api.db.Insert(flag)
}
