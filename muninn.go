package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/julienschmidt/httprouter"

	"github.com/muscar/muninn-go/api"
	"github.com/muscar/muninn-go/db"
	"github.com/muscar/muninn-go/views"
)

func main() {
	databaseUrl := os.Getenv("DB_ADDR")
	databaseUser := os.Getenv("DB_USER")
	db := db.Init(databaseUrl, databaseUser)
	service := api.NewFlagApi(db)
	views := views.NewFlagViews(service)

	router := httprouter.New()
	router.GET("/flags/:name/", views.GetOne)
	router.GET("/flags/", views.GetAll)
	router.POST("/flags/", views.Post)

	fmt.Printf("muninn up & running\n")

	log.Fatal(http.ListenAndServe(":8080", router))
}
