package models

type Flag struct {
	Name        string   `json:"name"`
	Description string   `json:"description"`
	Enabled     bool     `json:"enabled"`
	Teams       []string `json:"teams",pg:"array"`
}
