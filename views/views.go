package views

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"

	"github.com/muscar/muninn-go/api"
	"github.com/muscar/muninn-go/models"
)

type FlagViews struct {
	service *api.FlagApi
}

func jsonResponse(w http.ResponseWriter, res interface{}, err error) {
	if err != nil {
		log.Print(err)
		http.Error(w, "Internal Server Error", 500)
	}
	json.NewEncoder(w).Encode(res)
}

func NewFlagViews(service *api.FlagApi) *FlagViews {
	return &FlagViews{service: service}
}

func (v *FlagViews) GetAll(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	flags, err := v.service.All()
	jsonResponse(w, flags, err)
}

func (v *FlagViews) GetOne(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	name := ps.ByName("name")
	flag, err := v.service.Get(name)
	if flag == nil {
		http.Error(w, "Not Found", 404)
	} else {
		jsonResponse(w, flag, err)
	}
}

func (v *FlagViews) Post(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	decoder := json.NewDecoder(r.Body)
	var flag models.Flag
	err := decoder.Decode(&flag)

	if err != nil {
		goto exit
	}

	err = v.service.Add(&flag)

exit:
	jsonResponse(w, nil, err)
}
