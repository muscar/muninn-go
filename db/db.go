package db

import (
	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"

	"github.com/muscar/muninn-go/models"
)

func createSchema(db *pg.DB) error {
	for _, model := range []interface{}{&models.Flag{}} {
		err := db.CreateTable(model, &orm.CreateTableOptions{
			IfNotExists: true,
		})
		if err != nil {
			return err
		}
	}
	return nil
}

func Init(addr string, user string) *pg.DB {
	db := pg.Connect(&pg.Options{
		Addr: addr,
		User: user,
	})

	err := createSchema(db)
	if err != nil {
		panic(err)
	}

	return db
}
