# Build image

FROM golang:1.8.1 

COPY . /go/src/github.com/muscar/muninn-go

ENV GOPATH /go

WORKDIR /go/src/github.com/muscar/muninn-go

RUN go get -x; go build -x .

# Production image

FROM ubuntu:zesty

WORKDIR /root/

COPY --from=0 /go/src/github.com/muscar/muninn-go/muninn-go .
COPY bin/wait-for-it.sh .
COPY bin/entrypoint.sh .

ENTRYPOINT ["/root/entrypoint.sh"]
